# `SubQueries` on HR DATABASE

> __1. Write a query to display the name ( first name and last name ) for those
employees who gets more salary than the employee whose ID is 163.__
    
        select * from employees;
        
        select first_name||' '||last_name as "FullName" , salary 
        from employees
        where salary > (select salary 
                        from employees 
                        where employee_id =163);
                        
                        
> __2.Write a query to display the name ( first name and last name ), salary, department id, 
job id for those employees who works in the same designation as the employee works whose id is 169.__
            select * from employees;
            select first_name||' '||last_name as "FullName" , salary , department_id, job_id
            from employees 
            where job_id = (select job_id 
                                    from employees 
                                    where employee_id =169);
                                    
                                    
> __3.Write a query to display the name ( first name and last name ), salary, department id for those employees 
who earn such amount of salary which is the smallest salary of any of the departments.__

    select first_name||' '||last_name as "FullName" , salary , department_id, department_name
    from employees
    natural join departments
    where salary in (select min(salary)
                    from employees
                    group by department_id);
                    
                    
                    
    select * from job_history;
    select * from employees;
    select * from jobs;
    
> 12.__create a report that list the employee id and job id of those employees who 
        currently have a job tiltle that is same as the job title when they were initially hired by the company
        (They changed jobs but have now gone back to doing thier original jobs).__
   

------------
    select employee_id ,job_id , first_name
    from employees 
    where employee_id in (select employee_id from employees  minus select employee_id from job_history)
    union
    (select employee_id , job_id , first_name
    from employees
    intersect
    select j.employee_id , j.job_id ,e.first_name
    from employees e 
    join job_history j 
    on e.employee_id= j.employee_id
    );
    
`another way of minimal output`   

    select employee_id ,job_id 
    from employees 
    where employee_id in (select employee_id from employees  minus select employee_id from job_history)
    union
    (select employee_id , job_id
    from employees
    intersect
    select employee_id , job_id
    from job_history
    );
    
--------------
    select * from employees;

    Describe employees;
    select distinct job_id
    from employees;
    
    
> __The HR department needs a query to display all unique job codes from the 
EMPLOYEES table.__

    select distinct job_id 
    from employees;
    
> __The HR department wants more descriptive column headings for its report on employees. 
Copy the statement from lab_01_07.sql to the SQL Developer text box. Name the 
column headings Emp#, Employee, Job, and HireDate, respectively. Then run your 
query again.__

        SELECT employee_id "Emp #", last_name "Employee", 
       job_id "Job", hire_date "Hire Date" 
        FROM   employees; 
    
>__The HR department has requested a report of all employees and their job IDs. Display the 
last name concatenated with the job ID (separated by a comma and space) and name the column Employee and Title.__

    select * from employees;
    select last_name||', '||job_id "Employee and Title"
    from employees;
    
> __To familiarize yourself with the data in the EMPLOYEES table, create a query
to display all the data from the EMPLOYEES table.
Separate each column output with a comma. Name the column THE_OUTPUT.__

    select Employee_id||','||First_name||','||Last_name||','||Email||','||Phone_number||','||Hire_date||','||job_id||','||salary||
    commission_pct||','||Manager_Id||','||Department_id as The_ouput
    from employees;
    
> __Because of budget issues, the HR department needs a report that displays the last name and 
salary of employees earning more than $12,000. Place your SQL statement in a text file named 
lab_02_01.sql. Run your query. __

    select last_name, salary 
    from employees
    where salary >=12000;
    
> __Create a report that displays the last name 
and department number for employee number 176.__

    select last_name, department_id 
    from employees
    where employee_id =176;
             
    select distinct min(salary), first_name
    from employees
    group by first_name;

    select min(salary) from employees;


> __4. Write a query to display the employee id, employee name (first name and last name ) for all employees 
who earn more than the average salary.__
    
    select * from employees;
    select employee_id, first_name||' '||last_name Employee
    from employees
    where salary > 
            (select avg(salary) from employees)
    order by employee_id;
    
`There is a "error group function" not allowed here`

> Cause : We cannot use aggregrate function in where Clause.

        select employee_id, first_name||' '|| last_name "Employee-Name", avg(salary)
        from employees
        where salary> avg(salary)
        group by employee_id;
        
> __5. Write a query to display the employee name ( first name and last name ),
employee id and salary of all employees who report to Payam.__


        select * from employees;
        select employee_id, first_name||' '|| last_name "Employee-Name", salary
        from employees
        where manager_id =(Select employee_id from employees
                            where first_name='Payam')
                            
> __6.Write a query to display the department number, name ( first name and last name ), 
job and department name for all employees in the Finance department.__

    
        select e.department_id, e.first_name||' '||e.last_name name, e.job_id Job , d.Department_name 
        from employees e
        join departments d
        on d.department_id =e.department_id
        where d.Department_name in (
                select Department_name from employees
                where department_name='Finance');
            
`We cannot use = operator in query because it return multiple row`
>  Error : when we use '=' operator then error will be flashed out 'single-row subquery returns more than one row'.

>  `Action` : Use ANY, ALL, IN, or NOT IN to specify which values to compare or reword the query so only one row is retrieved. 
    
    
    > __7.Write a query to display all the information of an employee 
whose salary and reporting person id is 3000 and 121 respectively.__

    SELECT * FROM EMPLOYEES
    WHERE SALARY IN (SELECT SALARY FROM EMPLOYEES
                        WHERE SALARY = 3000)
    AND MANAGER_ID IN (SELECT MANAGER_ID FROM EMPLOYEES
                        WHERE MANAGER_ID = 121);
    
`OR`

    SELECT * 
    FROM employees 
    WHERE (salary,manager_id) IN 
                    (SELECT 3000,121
                    from employees);
                    

> __8.Display all the information of an employee whose id is any of the number 134, 159 and 183.__
    
    SELECT * 
    FROM EMPLOYEES
    WHERE EMPLOYEE_ID IN (134,159,183);
    
> __9.Write a query to display all the information of the employees whose salary is within the range 1000 and 3000.__
    
    SELECT * 
    FROM EMPLOYEES 
    WHERE SALARY BETWEEN 1000 AND 3000;
    
> __10.Write a query to display all the information of the employees whose salary is within the range of smallest salary and 2500.__

    SELECT * 
    FROM EMPLOYEES 
    WHERE SALARY BETWEEN (SELECT MIN(SALARY) 
                    FROM EMPLOYEES) AND 2500;
                    
> __11.11. Write a query to display all the information of the employees who does not 
work in those departments where some employees works whose manager id within the range 100 and 200.__

    SELECT * 
    FROM EMPLOYEES 
    WHERE  DEPARTMENT_ID NOT IN (SELECT DEPARTMENT_ID 
    FROM DEPARTMENTS WHERE MANAGER_ID BETWEEN 100 AND 200);
    
    
    
> __12.Write a query to display all the information for those employees whose id is any id who earn the second highest salary.__

    SELECT * FROM EMPLOYEES
    WHERE EMPLOYEE_ID  IN (SELECT EMPLOYEE_ID FROM EMPLOYEES WHERE SALARY =(SELECT MAX(SALARY) FROM EMPLOYEES 
    WHERE SALARY <(SELECT MAX(SALARY) FROM EMPLOYEES)));
    

> __13.Write a query to display the employee name( first name and last name ) and hiredate 
for all employees in the same department as Clara. Exclude Clara.__

    SELECT * FROM EMPLOYEES;
    SELECT FIRST_NAME||' '||LAST_NAME , HIRE_DATE 
    FROM EMPLOYEES WHERE DEPARTMENT_ID =(SELECT DEPARTMENT_ID 
    FROM EMPLOYEES WHERE FIRST_NAME ='Clara')
    AND FIRST_NAME <>'Clara';

select * from employees;





        