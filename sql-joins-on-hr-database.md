# SQL JOINS ON HR DATABASE

>  1. __Write a query in SQL to display all departments including those where does not have any employee.__

    select e.first_name , e.last_name  , d.department_name
    from employees e
    right outer JOIN departments d
    using (department_id );


> 2. __Write a query in SQL to display the first and last name and salary for those employees who earn less than the employee earn whose number is 182.__

    select e.first_name, e.last_name ,e.salary
    from employees e
    join employees s
    on e.salary < s.salary 
    and s.employee_id =182;
    
`This is the format of the self join`

> 3. __Write a query in SQL to display the first name of all employees including the first name of their manager.__

    select * from employees;
    
    select e.employee_id,e.first_name as "Employee name" , s.first_name as "manager name"
    from employees e
    join employees s
    using (employee_id ,manager_id);
    
    
[Using keyword References](https://lornajane.net/posts/2012/sql-joins-with-on-or-using)

[Using keyword stack Overfloow](https://stackoverflow.com/questions/13750152/using-keyword-vs-on-clause-mysql)

__An identifier is the representation within the language of items created by the user, as opposed to language keywords or commands.__
`OR`
__An Identifier is essentially a name of a database, table, or table column__.

> 4. __Write a query in SQL to display the department name, city, and state province for each department.__

    select * from departments;
    select * from locations;
    
    select d.department_name , l.city, l.state_province
    from departments d
    left outer join locations l
    on d.location_id  =l.location_id;
    
    
*Trying by __USING__ keyword*
    
    select d.department_name , l.city , l.state_province
    from departments d 
    left outer join locations l 
    using (location_id);

`Worked`



> __5. Write a query in SQL to display the first name, last name, department number and name,
for all employees who have or have not any department.__

    select * from departments;
    select * from employees;

    SELECT e.FIRST_NAME, e.LAST_NAME , d.DEPARTMENT_ID, d.DEPARTMENT_NAME
    FROM EMPLOYEES e
    LEFT OUTER JOIN DEPARTMENTS d
    ON e.DEPARTMENT_ID= d.DEPARTMENT_ID;
    
> __6. Write a query in SQL to display the first name of all employees and the first name of their manager
including those who does not working under any manager.__

    SELECT * FROM EMPLOYEES;
    SELECT e.FIRST_NAME as "Employee Name", m.FIRST_NAME AS "Manager Name"
    FROM EMPLOYEES e
    LEFT OUTER JOIN EMPLOYEES m
    ON e.EMPLOYEE_ID = m.MANAGER_ID;
    
    
> __7. Write a query in SQL to display the first name, last name, and department number for those 
employees who works in the same department as the employee who holds the last name as Taylor.__

    SELECT * FROM EMPLOYEES;
    
    SELECT e.FIRST_NAME , e.LAST_NAME ||' works in  department '|| e.DEPARTMENT_ID 
    FROM EMPLOYEES e
    JOIN EMPLOYEES s
    ON e.DEPARTMENT_ID = s.DEPARTMENT_ID 
    AND e.LAST_NAME ='Taylor'
    order by e.department_id ;
    
> __8. Write a query in SQL to display the job title, department name, full name 
(first and last name ) of employee, and starting date for all the jobs which started 
on or after 1st January, 2001 and ending with on or before 31 August, 2006__

`QUERYING WITHIN QUERY WORKING AS I THINK`

    SELECT  a.JOB_TITLE ,e.FIRST_NAME || ' '||e.LAST_NAME AS "FULL NAME" , e.JOB_ID AS "DEPARTMENT NAME", e.HIRE_DATE
    FROM EMPLOYEES e
    JOIN JOB_HISTORY j
    ON e.EMPLOYEE_ID = j.EMPLOYEE_ID
    JOIN JOBS a
    ON j.JOB_ID = a.JOB_ID
    AND e.HIRE_DATE IN (SELECT HIRE_DATE 
        FROM EMPLOYEES
        WHERE HIRE_DATE > ='01-JAN-01'
        AND HIRE_DATE <='31-AUG-06');
    
    
`Another way of same result`
    
    SELECT  a.JOB_TITLE ,e.FIRST_NAME || ' '||e.LAST_NAME AS "FULL NAME" , e.JOB_ID AS "DEPARTMENT NAME", e.HIRE_DATE
    FROM EMPLOYEES e
    JOIN JOB_HISTORY j
    ON e.EMPLOYEE_ID = j.EMPLOYEE_ID
    JOIN JOBS a
    ON j.JOB_ID = a.JOB_ID
    WHERE e.HIRE_DATE >= '01-JAN-01'
    AND e.HIRE_DATE <='31-AUG-06';
    
    
> __9. Write a query in SQL to display job title, full name (first and last name ) of employee, 
and the difference between maximum salary for the job and salary of the employee.__

    SELECT * FROM JOBS;
    SELECT * FROM EMPLOYEES;
    select * from job_history;
    
    SELECT j.JOB_TITLE, e.FIRST_NAME ||' ' || e.LAST_NAME AS "Employee Name" ,(j.MAX_SALARY-e.SALARY)as "Salary Difference"
    FROM EMPLOYEES e
    JOIN JOBS j 
    USING (JOB_ID);
    
`Another way`

    SELECT JOB_TITLE, FIRST_NAME ||' ' || LAST_NAME AS Employee_Name,
    max_salary-salary as Salary_Diffrence
    FROM employees
        natural join jobs;
    
    
    SELECT job_title, first_name || ' ' || last_name AS Employee_name, 
	max_salary-salary AS salary_difference 
	FROM employees 
		NATURAL JOIN jobs;
        
    
> __10. Write a query in SQL to display the name of the department, 
average salary and number of employees working in that department who got commission.__

    select * from employees;
    select * from departments;
    
    
    SELECT DEPARTMENT_NAME, AVG(SALARY),COUNT(EMPLOYEE_ID), COUNT(COMMISSION_PCT)
    FROM DEPARTMENTS 
    JOIN EMPLOYEES 
    USING (DEPARTMENT_ID)
    GROUP BY DEPARTMENT_NAME;
    
> `GROUP BY`
* GROUP BY : It will aggregate records by the specified columns which allows you to
perform aggregation functions on non-grouped columns (such as SUM, COUNT, AVG, etc).

`"Group By" clause is used for getting aggregate value (example: count of, sum of) in one or more columns with reference to a distinct column in a table.` 

`"Order By" clause is used to sort the resulting rows in the order of specified column or columns.`

> `ORDER BY`
* ORDER BY : It alters the order in which items are returned.

[Diffrence between __ORDER BY__ and __GROUP BY__ clause](https://www.quora.com/What-is-the-difference-between-group-by-and-order-by-in-SQL)

 
> __11.Write a query in SQL to display the full name (first and last name ) of employee,
and job title of those employees who is working in the department which ID is 80.__

    SELECT 
    first_name || ' ' || last_name AS Employee_name,Job_title
    from employees
    join jobs
    using(job_id)
    where department_id =80;
    
*Another Way*

    SELECT 
    first_name || ' ' || last_name AS Employee_name,Job_title
    from employees
    natural join jobs
    where department_id =80;
    
`Don't Use **ON** Clause on Natural Join`


> __12.Write a query in SQL to display the name of the country, city, and the departments which are running there.__

    select * from countries;
    select * from locations;
    SELECT * FROM DEPARTMENTS;
    
    SELECT (COUNTRY_NAME),CITY ,DEPARTMENT_NAME
    FROM COUNTRIES
    JOIN LOCATIONS USING(COUNTRY_ID)
    JOIN DEPARTMENTS USING(LOCATION_ID);
    
    
    
> __18. Write a query in SQL to display department
name and the full name (first and last name) of the manager.__

    select  * from employees;
    select * from departments;
    
    
    SELECT department_name, first_name || ' ' || last_name AS name_of_manager, D.manager_id
	FROM departments D 
		JOIN employees E 
			ON (D.manager_id=E.employee_id);
            
`The manager is also the employee that is assigned to employees(employee_id) table so mapping of two values gives the desired output`


    select * from employees;
    select distinct e.first_name , e.last_name,s.manager_id
    from employees e
    join employees s 
    on e.employee_id =s.manager_id
    order by s.manager_id;
    
`Self Join of two tables using Distinct Keyword`
    
    create view  calculated_salary as
    SELECT j.JOB_TITLE, e.FIRST_NAME ||' ' || e.LAST_NAME AS "Employee Name" ,(j.MAX_SALARY-e.SALARY)as "Salary Difference"
    FROM EMPLOYEES e
    JOIN JOBS j 
    USING (JOB_ID);
    
> __19. Write a query in SQL to display job title and average salary of employees.__
        
        select * from jobs;
        select * from employees;
    
        select job_title, avg(salary) 
        from employees
        natural join jobs
        group by job_title
        order by avg(salary);
        
> __13.Write a query in SQL to display the details of jobs
which was done by any of the employees who is presently earning a salary on and above 12000.__

    select * from jobs;
    select * from employees;
    
    select job_title , first_name||' '||last_name as "Employee Name" , salary
    from employees 
    natural join jobs
    where salary >=12000;
    

> __14.Write a query in SQL to display the country name,
city, and number of those departments where at leaste 2 employees are working.__

    select * from countries;
    select * from locations;
    select * from employees;
    select * from departments;
    
        SELECT country_name,city, COUNT(department_id)
        FROM countries 
            JOIN locations USING (country_id) 
            JOIN departments USING(location_id) 
    WHERE department_id IN 
        (SELECT department_id 
            FROM employees 
         GROUP BY department_id 
         HAVING COUNT(department_id)>=2)
    GROUP BY country_name,city;


> __15.Write a query in SQL to display the department name, full name (first and last name) of manager, and their city.__

    select * from employees;
    select * from departments;
    select * from locations;
    
    select distinct department_name ,first_name||' '||last_name as "Full Name" ,city
    from departments D
    join employees E
    on (D.manager_id = E.employee_id)
        join locations using(location_id)
    order by city;
    
### Extras 

    select department_name
    from departments;
    
    select replace('control and credit','and','should')
    from departments;
    
    select substring ('Finance',1,3) 
    from departments;
    select * from departments
    select reverse(department_name) 
    from departments;
    
    select replicate(department_name ,4)
    from departments;
    
Select * from employees;

select first_name ||' earns $'||salary 
from employees;


## Continued
> __16.Write a query in SQL to display the employee ID, job name, 
number of days worked in for all those jobs in department 80.__

    select * from jobs;
    select * from job_history;
    
    select employee_id ,job_title,end_date-start_date as "Working Days"
    from job_history 
    natural join jobs
    where department_id =80;

    select *from locations cross join 
    job_history;
    
> __17.Write a query in SQL to display the details of jobs whichwas done
by any of the employees who is presently earning a salary on and above 12000.__ 

    select * from employees;
    select * from jobs;
    
    select job_title , first_name ||' ' ||last_name as "Fullname", salary 
    from employees
    natural join jobs
    where salary  >=12000;
    
    
> __Write a query in SQL to display the full name (first and last name),
and salary of those employees who working in any department located in London.__

    select * from departments;
    select * from locations;
    select * from employees;
    
    select first_name||' '||last_name as "Fullname", salary , department_name 
    from employees
        join departments using (department_id)
        join locations using (location_id)
    where city ='London';
    
    
## End Of HR Database on `JOINS`
    
    

    
    